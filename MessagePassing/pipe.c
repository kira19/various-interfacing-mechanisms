#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<string.h>
#include <sys/wait.h>
#define P1_READ     0       
#define P2_WRITE    1			 //parent p1
#define P2_READ     2			//child  p2
#define P1_WRITE    3
#define NUM_OF_PIPES   2

int main(int argc, char *argv[])
{
    int fd[2*NUM_OF_PIPES];
    int  length, i;
    char value[30],value2[30];
    pid_t pid;

  printf("Please enter a text to send from parent to child :" );
    scanf("%[^\n]%*c",value);

    //create all the descriptor pairs we need
    for (i=0; i<NUM_OF_PIPES; ++i)
    {
    //pipe(fd+(2));
        if (pipe(fd+(i*2)) < 0)
        {
            printf("Failed to instntiate pipes");
            exit(-1);
        }
    }
	 if ((pid = fork()) < 0)
    {
        printf("Failed to fork process");
        exit(-1);
    }
   
//child 
    if (pid == 0)
    {
        
        close(fd[P1_READ]);
        close(fd[P1_WRITE]);

// wait for parent to send us a value 
        length = read(fd[P2_READ], &value, strlen(value));
        if (length < 0)
        {
            printf("Child: Failed to read data from pipe");
            exit(-2);
       }else
        {
           
            printf("Child process: Received from parent %s\n", value);

             

            if (write(fd[P2_WRITE], &value2, strlen(value2)) < 0)
            {
                printf("Child: Failed to send response value");
                exit(-1);
            }else{
            printf("Child process : Sending %s back to parent \n", value2);
            }
        }
        close(fd[P2_READ]);
        close(fd[P2_WRITE]);

        exit(-3);
    }
//parent 
    	
    close(fd[P2_READ]);
    close(fd[P2_WRITE]);
    //value= malloc(sizeof())
   
    // send a value to the child
    
    write(fd[P1_WRITE], &value, strlen(value));
    
    printf("Parent process : Sending %s to child\n", value);
    
    // now wait for a response
    length = read(fd[P1_READ], &value, strlen(value));
    if (length < 0)
    
        printf("Parent: failed to read value from pipe");
        exit(-1);
    }
    else
    {
        // report what we received
        printf("Parent Recived  : Recived from child  %s \n" ,value2);
    }
    
    printf("Please enter a text to send to parent to child  :" );
				scanf("%[^\n]%*c",value2);

    // close down remaining descriptors
    close(fd[P1_READ]);
    close(fd[P1_WRITE]);

    
    wait(NULL);

    return 0;
}
